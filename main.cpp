#include <bits/stdc++.h>
#include<string>
using namespace std;

string ZERO = "0";
string ONE = "1";
int MAXSIZE = 3000;

struct Divide{
    string quotient,mod;
};

template <typename T> 
class Stack{
    private:
        T *_stack;
        int _top;
        int _capacity;
    public:
        Stack(int size = MAXSIZE){
            _stack = new T[size];
            _capacity = size;
            _top = -1;
        }
        bool isFull(){
            if(_top == _capacity -1){
                return true;
            }
            return false;
        }
        bool isEmpty(){
            if(_top == -1){
                return true;
            }
            return false;
        }
        void push(T val){
            if(isFull()) {
                cout << "stack overflow\n";
                return;
            }
            _stack[++_top] = val;

        }
        T pop(){
            if(isEmpty()){
                cout << "stack empty\n";
                exit(0);
            }
            return _stack[_top--];
        }
        T peek(){
            return _stack[_top];
        }

};

class BigInt{
    private:
        string _numbers;
        int length() const {
            return _numbers.size();
        }
        string reverse(string& str){
            int i,len=0,n;
            char temp;
            len=str.size();
            n=len-1;
            for(i =0;i <=(len/2)-1; i++) {
                temp=str[i];
                str[i]=str[n];
                str[n]=temp;
                n--;
            }
            return str;
        }
    public:
        string getBigInt(){
            return _numbers;
        }
        BigInt(string& s){
            int length = s.size();
            for(int i=length -1 ;i>=0;i--){
                _numbers.push_back(s[i]);
            }
        }
        BigInt(const char* s){
            int length = strlen(s);
            for(int i=length -1 ;i>=0;i--){
                _numbers.push_back(s[i]);
            }
        }
        string operator + (const BigInt& b){
            string sum = "";
            int thisLen = length();
            int bLen = b.length();
            int maxLen = max(thisLen,bLen);
            int i=0,carry=0,x,y,s;
            while(i<maxLen){
                x = (i < thisLen ? _numbers[i]: '0') - '0';
                y = (i < bLen ? b._numbers[i] : '0') - '0';
                s = x + y + carry;
                carry = s / 10;
                sum.push_back((s % 10) + '0');
                i++;
            }
            if(carry > 0){
                sum.push_back(carry+'0');
            }
            string res = reverse(sum);
            res.erase(0, res.find_first_not_of('0'));
            if(res.empty()) return ZERO;
            return res;
        }
        string operator - (const BigInt& b){
            string diff = "";
            int thisLen = length();
            int bLen = b.length();
            int maxLen = max(thisLen,bLen);
            int i=0,borrow=0,d,x,y;
            while(i<maxLen){
                x = (i < thisLen ? _numbers[i]: '0') - '0';
                y = (i < bLen ? b._numbers[i] : '0') - '0';
                d = x - y - borrow;
                if(d<0){
                    d += 10;
                    borrow = 1;

                }else{
                    borrow = 0;
                }
                diff.push_back(d + '0');
                i++;
            }
            string res = reverse(diff);
            res.erase(0, res.find_first_not_of('0'));
            if(res.empty()) return ZERO;
            return res;
        }

        string operator * (const BigInt& b){
            int thisLen = length();
            int bLen = b.length();
            int maxLen = max(thisLen,bLen);
            string partPrdct,tempAdd = ZERO;
            int x,y,carry,prd;
            for(int i=0;i<bLen;i++){
                partPrdct = "";
                int placeValue = i;
                while(placeValue--){
                    partPrdct.push_back('0');
                }
                y = b._numbers[i] - '0';
                carry = 0;
                for(int j=0;j<thisLen;j++){
                    x = _numbers[j] - '0';
                    prd =( x * y) + carry;
                    if(prd > 9){
                        if( j == thisLen -1){
                            string temp = to_string(prd);
                            partPrdct.push_back(temp[1]);
                            partPrdct.push_back(temp[0]);
                        }else{
                            carry = prd / 10;
                            partPrdct.push_back((prd % 10) + '0');
                        }
                    }else{
                        partPrdct.push_back(prd + '0');
                        carry = 0;
                    }
                }
                partPrdct = reverse(partPrdct);
                tempAdd = BigInt(partPrdct) + BigInt(tempAdd);
            }
            tempAdd.erase(0, tempAdd.find_first_not_of('0'));
            if(tempAdd.empty()) return ZERO;
            return tempAdd;
        }
        bool operator < (const BigInt& b){
            int thisLen = length();
            int bLen = b.length();
            if(thisLen != bLen) return thisLen < bLen;
            int i = thisLen;
            while(i--){
                if(_numbers[i]!=b._numbers[i]){
                    return _numbers[i] < b._numbers[i];
                }
            }
            return false;
        }
        bool operator == (const BigInt& b){
            int thisLen = length();
            int bLen = b.length();
            if(thisLen != bLen) return false;
            int i = thisLen;
            while(i--){
                if(_numbers[i]!=b._numbers[i]){
                    return false;
                }
            }
            return true;
        }
        Divide operator / (const BigInt& b){
            Divide d;
            if(*this < b){
                d.mod = reverse(_numbers);
                d.quotient = ZERO;
                return d;
            }
            if(b._numbers == ZERO){
                cout << "Denominator cant be 0";
                exit(0);
            }
            int thisLen = length();
            int bLen = b.length();
            BigInt partNum(ZERO);
            int i = thisLen -1;
            BigInt ten("10");
            string tempA,tempB,tempC,tempD;
            string quotient = "";
            while(true){
                tempA = i==thisLen -1 ? ZERO : partNum * ten;
                string chr(1,_numbers[i]);
                tempB = BigInt(tempA) + BigInt(chr);
                BigInt num(tempB);
                if(!(num < b)){
                    break;
                }
                partNum = num;
                i--;
            }
            while(i>=0){
                tempA = partNum * ten;
                string chr(1,_numbers[i]);
                tempB = BigInt(tempA) + BigInt(chr);
                BigInt num(tempB);
                partNum = num;

                int digit = 9;
                string stringDigit = to_string(digit);
                BigInt bigDigit(stringDigit);
                tempC = bigDigit * b;
                while(partNum < BigInt(tempC)){
                    digit--;
                    stringDigit = to_string(digit);
                    bigDigit = BigInt(stringDigit);
                    tempC =  bigDigit * b;   
                }
                tempD = partNum - tempC;
                quotient.push_back(digit + '0');
                partNum =BigInt(tempD);
                i--;
            }
            d.quotient = quotient;
            d.mod = reverse(partNum._numbers);
            return d;
        }
};


string factorial(string s){
    string zero = ZERO,one=ONE;
    string fact=ONE;
    s.erase(0, s.find_first_not_of('0'));
    if(s == zero || s == one){
        fact = ONE;
        return fact;
    }
    BigInt bi(s);
    string minusOneFact = factorial(bi - BigInt(ONE));
    return bi * BigInt(minusOneFact);
}

string exponentiation(string base, string pow){
    string one=ONE,mul;
    if(pow == one){
        return base ;
    }
    struct Divide d = BigInt(pow) / BigInt("2");
    if(d.mod == ZERO) {
        mul = exponentiation(base,d.quotient);
        return BigInt(mul) * BigInt(mul);
    }else{
        string p = BigInt(pow) - BigInt(ONE);
        mul = exponentiation(base,p);
        return BigInt(mul) * BigInt(base);
    }
}

string hcf(string a , string b){
    if(b==ZERO){
        return a;
    }else{
        string newB = (BigInt(a) / BigInt(b)).mod;
        return hcf(b,newB);
    }
}

int getPrecedence(char c){
    switch(c){
        case '-':
            return 1;
        case '+':
            return 1;
        case 'x':
            return 2;
        default:
            return 0;
    }
}

string doCalculation(string& a , string& b, char op){
    switch(op){
        case '+':
            return BigInt(a) + BigInt(b);
        case '-':
            return BigInt(a) - BigInt(b);
        case 'x':
            return BigInt(a) * BigInt(b);
        default :
            return "";
    }
}

string evaluate(string exprn){
    int i=0;
    int length = exprn.size();
    string num;
    Stack<string> oprnd(200);
    Stack<char> oprtr(200);
    char curChr = exprn[i];
    while(curChr!='\0'){
        int type = getPrecedence(exprn[i]);
        if(type == 0) num+=curChr;
        else{
            oprnd.push(num);
            num="";
            if(oprtr.isEmpty()){
                oprtr.push(curChr);
            }else{
                char topOpr = oprtr.peek();
                while(!oprtr.isEmpty() && getPrecedence(topOpr) >= type){
                    char op = oprtr.pop();
                    string b = oprnd.pop();
                    string a = oprnd.pop();
                    string res = doCalculation(a,b,op);
                    oprnd.push(res);
                }
                oprtr.push(curChr);
            }    
        }
        i++;
        curChr = exprn[i];
    }
    if(!num.empty()){
        oprnd.push(num);
    }
    while(!oprtr.isEmpty()){
        char op = oprtr.pop();
        string b = oprnd.pop();
        string a = oprnd.pop();
        string res = doCalculation(a,b,op);
        oprnd.push(res);
    }
    string res = oprnd.pop();
    if(oprnd.isEmpty()){
        return res;
    }else{
        cout<< "Invalid equation\n";
    }
    return "";
}

void printMenu();
void serveInput(int input);
int main(){
    int input=-1;
    while(input!=0){
        printMenu();
        cin >> input;
        serveInput(input);
    }
    return 0;
}
// to:do erase leading 0s from input
void serveInput(int input){
    switch(input){
        case 0: 
            return;
        case 1: {
            cout << "ENter the expression\n";
            string s;
            cin >> s;
            cout << evaluate(s);
            return;
        }
        case 2:{
            cout << "Enter the base and exponent\n";
            string a,b;
            cin >> a >> b;
            cout << exponentiation(a,b);
            return;
        }
        case 3:{
            cout << "Enter a and b\n";
            string a,b;
            cin >> a >> b;
            cout << hcf(a,b);
            return;
        }
        case 4:{
            cout << "Enter the number\n";
            string num;
            cin >> num;
            cout << factorial(num);
            return;
        }
        default:
            return;
    }
    return;
}

void printMenu(){
    cout << "\n-------------------------------------------------------\n";
    cout << "Select one of the options\n";
    cout << "0. Quit\n";
    cout << "1. Addition, Subtraction & Multiplication(Infix expression)\n";
    cout << "2. Exponentiation(a,b)\n";
    cout << "3. GCD(a,b)\n";
    cout << "4. Factorial(a)\n";
    cout << "\n-------------------------------------------------------\n";
}
